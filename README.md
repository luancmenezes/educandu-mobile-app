<strong>EXECUÇÃO</strong>
<br>
<strong>1) mobile</strong>
<br>
<p> Dentro da pasta 'mobile': </p>
<p>1.1) npm install</p>
<p>1.2) ionic serve</p>
<br>
<strong>2)Backend API</strong>
<br>
<p> Dentro da pasta 'educandu-backend': </p>
<p>2.1) npm install</p>
<p>2.2) npm start</p>
<br><br>

<h2>Regras desenvolvimento</h2>
<br>
<strong>Criação de issues</strong>
<p>1) Nome com a funcionalidade que irá desenvolver</p>
<p>2) Inserir label e responsável</p>
<br>
<h2>Criação de branchs</h2>
<p>1) Criar merge request através da issue</p>
<p>2) Testar as funcionalidades e fazer merge</p>
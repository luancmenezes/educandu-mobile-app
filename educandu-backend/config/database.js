var mongoose = require('mongoose');
const url = ('mongodb://localhost:27017/educandu');
mongoose.connect(url, { useNewUrlParser: true });
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
  console.log("Connection Succeeded");
});  

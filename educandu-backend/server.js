//conexão com banco obrigatório
require('./config/database')

// dependências
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')
const app = express()
const jwt = require('jsonwebtoken');

app.use(morgan('combined'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
require('./app/routes/usuarioRoutes.js')(app);
require('./app/routes/grupoRoutes.js')(app);
require('./app/routes/loginRoutes.js')(app);
// subindo api
app.listen(process.env.PORT || 3000)




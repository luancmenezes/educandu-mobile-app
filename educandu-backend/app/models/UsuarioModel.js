const mongoose = require('mongoose');

const UsuarioSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    date_birth: {
        type: String,
        required: true
    },
    institution: {
        type: String,
        required: true
    },
    course: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
},
{
    timestamps: true
});

module.exports = mongoose.model('Usuario', UsuarioSchema, 'users');
const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.Types.ObjectId;

const GrupoSchema = mongoose.Schema({
    institution: {
        type: String,
        required: true
    },
    discipline: {
        type: String,
        required: true
    },
    teacher: {
        type: String,
        required: true
    },
    semester: {
        type: String,
        required: true
    },
    id_user: {
        type: ObjectId,
        required: true
    },
},
{
    timestamps: true
});

module.exports = mongoose.model('Grupo', GrupoSchema, 'groups');
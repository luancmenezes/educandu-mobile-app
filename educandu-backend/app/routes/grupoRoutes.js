module.exports = (app) => {
    const grupo = require('../controllers/grupoController.js');
    
    // cadastrar usuarios
    app.post('/groups', grupo.create);

    // listar todos usuarios    
    app.get('/groups', grupo.findAll);
 
}


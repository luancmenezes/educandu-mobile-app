module.exports = (app) => {
    const usuario = require('../controllers/usuarioController.js');
    
    // cadastrar usuarios
    app.post('/usuarios', usuario.create);

    // listar todos usuarios    
    app.get('/usuarios', usuario.findAll);
 
}


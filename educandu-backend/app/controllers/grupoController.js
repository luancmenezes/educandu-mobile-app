const Grupo = require('../models/GrupoModel.js');

// Cria e exporta função para cadastro
exports.create = (req, res) => {
    // Validate request
    if(!req.body.institution) {
       return res.status(400).send({
           message: "Instituição não pode ser vazio"
       });
    }

    if(!req.body.discipline) {
        return res.status(400).send({
            message: "Disciplina não pode ser vazio"
        });
     }

    if(!req.body.teacher) {
        return res.status(400).send({
            message: "Professor não pode ser vazio"
        });
    }

    if(!req.body.semester) {
        return res.status(400).send({
            message: "Semestre não pode ser vazio"
        });
    }

    if(!req.body.id_user) {
        return res.status(400).send({
            message: "id usuarui não pode ser vazio"
        });
    }

    // Cadastrando usuário
    const grupo = new Grupo({
        institution: req.body.institution,
        discipline: req.body.discipline, 
        teacher: req.body.teacher,
        semester: req.body.semester,
        id_user: req.body.id_user,
    });
   
    // Salvando
    grupo.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Erro ao cadastrar grupo."
        });
    });     
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Grupo.find()
    .then(grupos => {
        res.send(grupos);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Erro ao listar grupos."
        }); 
    });
};

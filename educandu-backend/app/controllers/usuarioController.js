const Usuario = require('../models/UsuarioModel.js');
const serverJWT_Secret = 'kpTxN=)7mX3W3SEJ58Ubt8-';
const jwt = require('jsonwebtoken');
// Cria e exporta função para cadastro

const verificaEmail = email => {
    Usuario.find({email: email}, function(err, existe) {
        if(err)
            return false;
        if(existe)
            return true;    
    }) 
}

exports.create = (req, res) => {
    // Validate request
    if(!req.body.name) {
       return res.status(400).send({
           message: "Nome não pode ser vazio"
       });
    }

    if(!req.body.date_birth) {
        return res.status(400).send({
            message: "Data não pode ser vazio"
        });
     }

    if(!req.body.institution) {
        return res.status(400).send({
            message: "Instituição não pode ser vazio"
        });
    }

    if(!req.body.course) {
        return res.status(400).send({
            message: "Curso não pode ser vazio"
        });
    }

    if(!req.body.email) {
        return res.status(400).send({
            message: "Email não pode ser vazio"
        });
    }

     if(!req.body.password) {
        return res.status(400).send({
            message: "Senha não pode ser vazio"
        });
    }

    // Cadastrando usuário
    const user = new Usuario({
        name: req.body.name,
        date_birth: req.body.date_birth, 
        institution: req.body.institution,
        course: req.body.course,
        email: req.body.email,
        password: req.body.password
    });

    if(!verificaEmail(req.body.email)) {
        // Salvando
        user.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Erro ao cadastrar usuário."
            });
        });
    }
    else {
        res.status(500).send({
            message: "E-mail já cadastrado."
        })
    }
   
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Usuario.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Erro ao listar usuários."
        }); 
    });
};

const Usuario = require('../models/UsuarioModel.js');
const serverJWT_Secret = 'kpTxN=)7mX3W3SEJ58Ubt8-';
const jwt = require('jsonwebtoken');
// Cria e exporta função para cadastro
exports.login = (req, res) => {
    // Validate request

    //objeto com os dados do formulario
    let user = {
        email: req.body.email,
        password: req.body.password
    }
   
    Usuario.find({email: user.email, password: user.password})
    .then(users => {

        //criando objeto com os campos que serao utilizados para criar o token, sem o password
        const userLogin = {
            _id: users[0]._id,
            name: users[0].name,
            date_birth: users[0].date_birth,
            institution: users[0].institution,
            course: users[0].course,
            email: users[0].email
        };

        console.log(users[0]._id)

        //criando o token    
        const token = jwt.sign(userLogin, serverJWT_Secret);
        res.status(200).send({
            user: userLogin,
            token: token
        });
    }).catch(err => {
        res.status(403).send({
            errorMessage: 'Email e/ou senha inválido(s)'
        })
        console.log(err);
    })
};

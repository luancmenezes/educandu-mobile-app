
import { IonicPage, NavController } from 'ionic-angular';
import { Component } from '@angular/core';
import { CreatePostPage } from '../create-post/create-post';
import { CommentPage } from '../comment/comment';


@IonicPage()
@Component({
  selector: 'page-group',
  templateUrl: 'group.html',
})
export class GroupPage {
  
  //const currentSlide = this.slides[slider.getActiveIndex()];
  
  selectedSegment: string;
  slides: any;
  
  constructor(public navCtrl: NavController) {
    this.selectedSegment = "second";
    this.slides = [];
  }
  createPost(){
    this.navCtrl.push(CreatePostPage);
  }
  createComment(){
    this.navCtrl.push(CommentPage);
  }
  onSegmentChanged(segmentButton) {
    console.log("Segment changed to", segmentButton.value);
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id === segmentButton.value;
    });
    
  }

  onSlideChanged(slider) {
    console.log('Slide changed');
    const currentSlide = this.slides[slider.activeIndex];
    this.selectedSegment = currentSlide.id;
  }
}

import { CreateGroupPage } from './../create-group/create-group';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  createGroup(){
    console.log("Teste");
    this.navCtrl.push(CreateGroupPage);
  }
}

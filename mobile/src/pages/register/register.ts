import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MenuPage } from '../../pages/menu/menu';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})

export class RegisterPage {

   public form: FormGroup;

   // Initialise module classes
   constructor(
               public http       : HttpClient,
               public fb         : FormBuilder,
               public navCtrl    : NavController,   
               public toastCtrl  : ToastController)
   {
      // Create form builder validation rules
      this.form = fb.group({
         name:        ["", Validators.required],
         date_birth:  ["", Validators.required],
         institution: ["", Validators.required],
         course:      ["", Validators.required],
         email:       new FormControl('', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
         password:    new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(4)])
      });
   }

  createEntry(payload) {

    const  options 	= payload;
    const  url      = 'http://localhost:3000/usuarios';  

    return this.http.post(url, options)
    .subscribe((data : any) => {
      this.sendNotification(`Cadastrado com sucesso!`);
      this.navCtrl.push(MenuPage);
    },
    (error : any) => {
      this.sendNotification('Falha ao cadastrar!');
      console.log(error)
    });
  }
   
  saveEntry() : void {

      const payload = {name:'', date_birth: '', institution: '', course: '', email: '', password: ''}
      payload.name = this.form.controls["name"].value;
      payload.date_birth = this.form.controls["date_birth"].value;
      payload.institution = this.form.controls["institution"].value;
      payload.course = this.form.controls["course"].value;
      payload.email = this.form.controls["email"].value;
      payload.password = this.form.controls["password"].value;

      this.createEntry(payload);   
   }

   sendNotification(message : string)  : void {

      let notification = this.toastCtrl.create({
          message       : message,
          duration      : 3000
      });

      notification.present();
   }
}

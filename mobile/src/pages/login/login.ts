import { Component, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { LoginService } from './login.service'

@IonicPage()
@Injectable()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  myColor: string = 'educandu'; 
  public form: FormGroup;
  loggedIn;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    public fb: FormBuilder,
    private loginService: LoginService,
    public toastr: ToastrService)  

   {
    this.loginService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });

    this.form = fb.group({
        email:       new FormControl('', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
        password:    new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(4)])
    });
  }    
  
  login()  {
    const payload = {email: '', password: ''}
    payload.email = this.form.controls["email"].value;
    payload.password = this.form.controls["password"].value;

    this.loginService.doLogin(payload);   
  }

  logout() {
    this.loginService.logout();
  }

  pushPage(): void { 
    this.navCtrl.push(RegisterPage);
  }

}

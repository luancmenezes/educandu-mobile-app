import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { LoginService } from './login.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    BrowserAnimationsModule,
    IonicPageModule.forChild(LoginPage),
  ],
  providers: [
    LoginService
  ]
})
export class LoginPageModule {}

import { Component, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IonicPage } from 'ionic-angular';
import { MenuPage } from '../menu/menu';
import { RegisterPage } from '../register/register';

@Injectable()

export class LoginService {
 
  constructor(
  //  public navCtrl: NavController,
    public http: HttpClient,
    public toastr: ToastrService)  

    {
      const jwtToken = this.getToken();
      this.loggedIn = new BehaviorSubject<boolean>(jwtToken ? true : false);
    }    
  
  loggedIn: BehaviorSubject<boolean>;

  getToken(): string {
    return window.localStorage['jwtToken'];
  }

  saveToken(token: string) {
    window.localStorage['jwtToken'] = token;
  }

  destroyToken() {
    window.localStorage.removeItem('jwtToken');
  }

  buildHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (this.getToken()) {
      headersConfig['Authorization'] = `Token ${this.getToken()}`;
    }
    return new HttpHeaders(headersConfig);
  }

  doLogin(payload) {
    const url = 'http://localhost:3000/login';
     this.http.post(url, {
      email: payload.email,
      password: payload.password
    }).subscribe((resp: any) => {
      this.loggedIn.next(true);
      this.saveToken(resp.token);
      this.toastr.success(resp && resp.user && resp.user.name ? `Welcome ${resp.user.name}` : 'Logged in!');
     // this.navCtrl.setRoot(MenuPage);
    }, (errorResp) => {
      this.loggedIn.next(undefined);
      errorResp.error ? this.toastr.error(errorResp.error.errorMessage) : this.toastr.error('An unknown error has occured.');
    });
  }

  logout() {
    this.destroyToken();
    this.loggedIn.next(false);
  }
  
}

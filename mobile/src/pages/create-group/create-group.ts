import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ToastController } from 'ionic-angular';
import { DecodedToken } from '../../methods/decodedToken';
import { LoginService } from '../menu/login.service';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-create-group',
  templateUrl: 'create-group.html',
})
export class CreateGroupPage {


  public form: FormGroup;
 
 
    // Initialise module classes
   constructor(public navCtrl    : NavController,
               public http       : HttpClient,
               public NP         : NavParams,
               public fb         : FormBuilder,
               public toastCtrl  : ToastController,
               private loginService: LoginService,
               public decoded: DecodedToken)
   {
      this.form = fb.group({
         discipline         : ["", Validators.required],
         semester           : ["", Validators.required],
         teacher            : ["", Validators.required]
      });
    }
 
 
    createEntry(group) 
    {
        const  options 	= group;
        const  url      = 'http://localhost:3000/groups';  

        return this.http.post(url, options)
        .subscribe((data : any) => {
        this.sendNotification(`Cadastrado com sucesso!`);
        this.navCtrl.push(HomePage);
        },
        (error : any) => {
        this.sendNotification('Falha ao cadastrar!');
        console.log(error)
        });
    }
    saveEntry() : void {      
            const userToken = this.loginService.getToken();  
            const user = this.decoded.parseJwt(userToken);
            const group = {institution:'', discipline: '', semester: '', teacher: '', id_user: ''}
            group.institution = user.institution,
            group.discipline  = this.form.controls["discipline"].value,
            group.semester    = this.form.controls["semester"].value,
            group.teacher     = this.form.controls["teacher"].value;
            group.id_user     = user._id;
            
            this.createEntry(group);
            
    }
 
    sendNotification(message : string)  : void 
   {
      let notification = this.toastCtrl.create({
          message       : message,
          duration      : 3000
      });
      notification.present();
   }
}


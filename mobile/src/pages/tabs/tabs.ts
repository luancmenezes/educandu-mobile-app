import { HomePage } from '../home/home';
import { Tb2Page } from '../tb2/tb2';
import { CreateGroupPage } from './../create-group/create-group';
import { Component } from '@angular/core';
import { ModalController, ViewController } from 'ionic-angular';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  firstTabIcon: string;
  tab1Root = HomePage;
  tab3Root = CreateGroupPage;
  tab2Root = Tb2Page;
  constructor(public modalCtrl : ModalController) {

    }
  // openModal(){
  //   console.log("entrou");
  //   let modalPage = this.modalCtrl.create(CreateGroupPage); 
  //   modalPage.present();
  // }    
  ionViewDidLoad() {
    this.firstTabIcon = 'first' // or 'first-changed'
  }
}

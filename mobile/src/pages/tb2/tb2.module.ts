import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Tb2Page } from './tb2';

@NgModule({
  declarations: [
    Tb2Page,
  ],
  imports: [
    IonicPageModule.forChild(Tb2Page),
  ],
})
export class Tb2PageModule {}

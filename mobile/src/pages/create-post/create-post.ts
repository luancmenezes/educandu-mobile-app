import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GroupPage } from '../group/group';

@IonicPage()
@Component({
  selector: 'page-create-post',
  templateUrl: 'create-post.html',
})
export class CreatePostPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  createPost(){
    this.navCtrl.push(GroupPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CreatePostPage');
  }

}

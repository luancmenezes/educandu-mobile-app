import { Component, Injectable } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../menu/login.service'
import { DecodedToken } from '../../methods/decodedToken'
import { FormGroup } from '@angular/forms';

@IonicPage()
@Injectable()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  // const currentSlide = this.slides[slider.getActiveIndex()];
  public form: FormGroup;

  selectedSegment: string;
  slides: any;
  nome: string;
  instituicao: string;
  email: string;
  
  constructor(public navCtrl: NavController, public http: HttpClient, private loginService: LoginService, public decoded: DecodedToken) {
    this.selectedSegment = "second";
    this.slides = [];
    const userToken = loginService.getToken();
    
    const user = this.decoded.parseJwt(userToken);
    this.nome = user.name;
    this.instituicao = user.institution;
  }

  onSegmentChanged(segmentButton) {
    console.log("Segment changed to", segmentButton.value);
    const selectedIndex = this.slides.findIndex((slide) => {
      return slide.id === segmentButton.value;
    });
    
  }

  onSlideChanged(slider) {
    console.log('Slide changed');
    const currentSlide = this.slides[slider.activeIndex];
    this.selectedSegment = currentSlide.id;
  }
}

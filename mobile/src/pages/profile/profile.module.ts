import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginService } from '../login/login.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfilePage } from './profile'
import { DecodedToken } from '../../methods/decodedToken'
import { FileTransfer } from '@ionic-native/file-transfer';



@NgModule({
  declarations: [
    ProfilePage
  ],
  imports: [
    BrowserAnimationsModule,
    IonicPageModule.forChild(ProfilePage),
  ],
  providers: [
    LoginService,
    DecodedToken,
    FileTransfer
  ]
})
export class ProfilePageModule {}

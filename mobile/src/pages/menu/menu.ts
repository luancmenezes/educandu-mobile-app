import { ProfilePage } from './../profile/profile';
import { Component, Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { LoginService } from './login.service'
import { RegisterPage } from '../register/register';
import { HttpClient } from '@angular/common/http';


@IonicPage()
@Injectable()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  myColor: string = 'educandu'; 
  public form: FormGroup;
  loggedIn;
  rootPage = TabsPage;
  selectedItem: any;
  icons: string[];
  namePages: string[];
  pages: string[];
  items: Array<{title: string,  icon: string, page: string}>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public fb: FormBuilder,
    private loginService: LoginService,
    private http: HttpClient

  ) {
    this.loginService.loggedIn.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });
    this.form = fb.group({
      email:       new FormControl('', [Validators.required, Validators.pattern("[^ @]*@[^ @]*")]),
      password:    new FormControl('', [Validators.required, Validators.maxLength(10), Validators.minLength(4)])
     });
     // If we navigated to this page, we will have an item available as a nav param
     this.selectedItem = navParams.get('item');

     // Let's populate this page with some filler content for funzies
     this.icons = ['person', 'construct', 'contacts', 'people', 'exit'];
     this.namePages = ['Meu Perfil', 'Configurações', 'Amigos', 'Grupos', 'Sair'];
     this.pages = ['ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage']

     this.items = [];
     for (let i = 0; i < 5; i++) {
       this.items.push({
         page: this.pages[i],
         title:  this.namePages[i],
         icon: this.icons[i],
        
       });
     }
   }
    
  itemTapped(event, item) {
     this.navCtrl.push(ProfilePage);
  }

  login()  {
    const payload = {email: '', password: ''}
    payload.email = this.form.controls["email"].value;
    payload.password = this.form.controls["password"].value;

    this.loginService.doLogin(payload);   
  }

  logout() {
    this.loginService.logout();
  }


  pushPage(): void { 
    this.navCtrl.push(RegisterPage);
  }
 
}

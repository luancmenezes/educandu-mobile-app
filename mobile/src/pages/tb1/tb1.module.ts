import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Tb1Page } from './tb1';

@NgModule({
  declarations: [
    Tb1Page,
  ],
  imports: [
    IonicPageModule.forChild(Tb1Page),
  ],
})
export class Tb1PageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupmapPage } from './groupmap';

@NgModule({
  declarations: [
    GroupmapPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupmapPage),
  ],
})
export class GroupmapPageModule {}

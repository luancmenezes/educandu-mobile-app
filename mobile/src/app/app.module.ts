import { CreateGroupPage } from './../pages/create-group/create-group';
import { ShrinkingSegmentHeader } from './../components/shrinking-segment-header/shrinking-segment-header';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MenuPage } from '../pages/menu/menu';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { Tb1Page } from '../pages/tb1/tb1';
import { Tb2Page } from '../pages/tb2/tb2';
import { RegisterPage } from '../pages/register/register';
import { ProfilePage } from '../pages/profile/profile';
import { HttpModule } from '@angular/http';
import { ToastrModule } from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LoginService } from '../pages/menu/login.service';
import { DecodedToken } from '../methods/decodedToken'
import { FileTransfer } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    MenuPage,
    TabsPage,
    Tb2Page,
    Tb1Page,
    RegisterPage,
    ProfilePage,
    ShrinkingSegmentHeader,
    CreateGroupPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    MenuPage,
    TabsPage,
    Tb1Page,
    Tb2Page,
    RegisterPage,
    ProfilePage,
    CreateGroupPage
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoginService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DecodedToken,
    FileTransfer
  ]
})
export class AppModule {}

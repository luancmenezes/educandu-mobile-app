export class User{

    constructor(
        public name: string,
        public date_birth: string,
        public institution: string,
        public course: string,
        public email: string,
        public uid: string
    ){

    }

}

webpackJsonp([0],{

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupPageModule", function() { return GroupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group__ = __webpack_require__(299);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var GroupPageModule = (function () {
    function GroupPageModule() {
    }
    GroupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__group__["a" /* GroupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__group__["a" /* GroupPage */]),
            ],
        })
    ], GroupPageModule);
    return GroupPageModule;
}());

//# sourceMappingURL=group.module.js.map

/***/ }),

/***/ 297:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CommentPage = (function () {
    function CommentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CommentPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CommentPage');
    };
    CommentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-comment',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\comment\comment.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Comentário</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-row>\n\n            <ion-col col-3>\n\n              <ion-avatar item-start>\n\n                <img src="assets/icon/doria.jpg">\n\n              </ion-avatar>\n\n            </ion-col>\n\n            <ion-col col-9>\n\n              <h4>Tiago</h4>\n\n              <p>I\'ve had a pretty messed up day. If we just...</p>\n\n            </ion-col>\n\n        </ion-row>\n\n      </ion-item>\n\n      <ion-item>\n\n          <ion-row>\n\n                <ion-col col-3>\n\n                  <ion-avatar item-start>\n\n                    <img src="assets/icon/doria.jpg">\n\n                  </ion-avatar>\n\n                </ion-col>\n\n                <ion-col col-9>\n\n                  <h4>Tiago</h4>\n\n                  <p>I\'ve had a pretty messed up day. If we just...</p>\n\n                </ion-col>\n\n            </ion-row>\n\n          </ion-item>\n\n    </ion-list>\n\n</ion-content>\n\n\n\n<ion-footer>\n\n  <ion-list inset>\n\n    <ion-item>\n\n      <ion-input type="text"></ion-input>\n\n      <button  item-right>Comentar</button>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-footer>'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\comment\comment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CommentPage);
    return CommentPage;
}());

//# sourceMappingURL=comment.js.map

/***/ }),

/***/ 298:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreatePostPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__group_group__ = __webpack_require__(299);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CreatePostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreatePostPage = (function () {
    function CreatePostPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CreatePostPage.prototype.createPost = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__group_group__["a" /* GroupPage */]);
    };
    CreatePostPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreatePostPage');
    };
    CreatePostPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-create-post',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\create-post\create-post.html"*/'<ion-header>\n\n\n\n    <ion-navbar>\n\n      <ion-title>Crie sua Postagem</ion-title>\n\n    </ion-navbar>\n\n  \n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding id="register">\n\n     <ion-grid>\n\n      <div>\n\n          <form >\n\n              <ion-list>\n\n                  <ion-item class="box-register">\n\n                      <ion-label stacked>Texto</ion-label>\n\n                      <ion-input\n\n                        class="register_input"\n\n                        type="text"\n\n                        placeholder="Título"\n\n                        \n\n                        ></ion-input>\n\n                  </ion-item>\n\n                  <ion-item>\n\n                      <ion-label stacked>Texto</ion-label>\n\n                      <ion-textarea ></ion-textarea>\n\n                    </ion-item>\n\n\n\n                  <button color="educandu_dark" ion-button block (click)="createPost()">Criar</button>\n\n            </ion-list>\n\n          </form>\n\n      </div>\n\n    </ion-grid>\n\n  \n\n  </ion-content>'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\create-post\create-post.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CreatePostPage);
    return CreatePostPage;
}());

//# sourceMappingURL=create-post.js.map

/***/ }),

/***/ 299:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_post_create_post__ = __webpack_require__(298);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__comment_comment__ = __webpack_require__(297);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GroupPage = (function () {
    function GroupPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.selectedSegment = "second";
        this.slides = [];
    }
    GroupPage.prototype.createPost = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__create_post_create_post__["a" /* CreatePostPage */]);
    };
    GroupPage.prototype.createComment = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__comment_comment__["a" /* CommentPage */]);
    };
    GroupPage.prototype.onSegmentChanged = function (segmentButton) {
        console.log("Segment changed to", segmentButton.value);
        var selectedIndex = this.slides.findIndex(function (slide) {
            return slide.id === segmentButton.value;
        });
    };
    GroupPage.prototype.onSlideChanged = function (slider) {
        console.log('Slide changed');
        var currentSlide = this.slides[slider.activeIndex];
        this.selectedSegment = currentSlide.id;
    };
    GroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-group',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\group\group.html"*/'<ion-header>\n\n  <ion-navbar color="educandu_dark">\n\n      <ion-title>\n\n        Grupo\n\n      </ion-title>\n\n  </ion-navbar>\n\n  \n\n  <shrinking-segment-header scrollArea="myContent" headerHeight="150">\n\n    <ion-grid class="grid_content">\n\n      <ion-row>\n\n        <ion-col col-2></ion-col>\n\n        <ion-col col-8 > <h2>Calculo A</h2></ion-col>\n\n        <ion-col col-2></ion-col>\n\n      </ion-row>    \n\n    </ion-grid>\n\n  </shrinking-segment-header>\n\n\n\n  <ion-toolbar color="educandu_light" mode="md">\n\n    <ion-segment class="segment" color="dark" mode="md" [(ngModel)]="selectedSegment"  >\n\n      <ion-segment-button  value="first">\n\n        <ion-icon name="folder-open"></ion-icon>\n\n      </ion-segment-button>\n\n      <ion-segment-button value="second">\n\n        <ion-icon name="contacts"></ion-icon>\n\n      </ion-segment-button>\n\n      <ion-segment-button value="third">\n\n        <ion-icon name="albums"></ion-icon>\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content >\n\n      <div [ngSwitch]="selectedSegment">\n\n          <ion-list *ngSwitchCase="\'first\'">\n\n              <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Cothing</h2>\n\n               </ion-item>\n\n               <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Watches</h2>\n\n               </ion-item>\n\n               <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="/img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Footwear</h2>\n\n               </ion-item>\n\n            </ion-list>\n\n          <ion-row *ngSwitchCase="\'second\'">\n\n              <ion-col col-12>\n\n                <ion-card>\n\n                  <ion-item>\n\n                    <ion-avatar item-start>\n\n                      <img src="assets/icon/perfil.jpg">\n\n                    </ion-avatar>\n\n                      <h2>Sahusa Carlos</h2>\n\n                      <p>18 de Janeiro</p>\n\n                  </ion-item>\n\n                  <ion-card-content>\n\n                    <ion-card-title>\n\n                      Recomendação de Livro\n\n                      </ion-card-title>\n\n                    <p>\n\n                      Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n\n                    </p>\n\n                  </ion-card-content>\n\n                  <ion-row>\n\n                      <ion-col>\n\n                        <button ion-button icon-left clear small>\n\n                          <ion-icon name="thumbs-up"></ion-icon>\n\n                          <div>12 Gostei</div>\n\n                        </button>\n\n                      </ion-col>\n\n                      <ion-col>\n\n                        <button ion-button icon-left clear small (click)="createComment()">\n\n                          <ion-icon name="text"></ion-icon>\n\n                          <div>4 Comentários</div>\n\n                        </button>\n\n                      </ion-col>\n\n                    </ion-row>\n\n                </ion-card>\n\n              </ion-col>\n\n              <ion-col col-12>\n\n                  <ion-card>\n\n                    <ion-item>\n\n                      <ion-avatar item-start>\n\n                        <img src="assets/icon/perfil.jpg">\n\n                      </ion-avatar>\n\n                        <h2>Sahusa Carlos</h2>\n\n                        <p>18 de Janeiro</p>\n\n                    </ion-item>\n\n                    <ion-card-content>\n\n                      <ion-card-title>\n\n                        Recomendação de Livro\n\n                        </ion-card-title>\n\n                      <p>\n\n                        Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n\n                      </p>\n\n                    </ion-card-content>\n\n                  </ion-card>\n\n                </ion-col>\n\n            </ion-row>\n\n          <ion-list *ngSwitchCase="\'third\'">\n\n              <ion-col col-12>\n\n                  <ion-card>\n\n                    <ion-item>\n\n                      <ion-avatar item-start>\n\n                        <img src="assets/icon/doria.jpg">\n\n                      </ion-avatar>\n\n                        <h2>Sahusa Carlos</h2>\n\n                        <p>18 de Janeiro</p>\n\n                    </ion-item>\n\n                    <ion-card-content>\n\n                      <ion-card-title>\n\n                          Campus Mobile\n\n                        </ion-card-title>\n\n                      <p>\n\n                        Para quem gosta de empreender\n\n                      </p>\n\n                    </ion-card-content>\n\n                  </ion-card>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                    <ion-card>\n\n                      <ion-item>\n\n                        <ion-avatar item-start>\n\n                          <img src="assets/icon/doria.jpg">\n\n                        </ion-avatar>\n\n                          <h2>Sahusa Carlos</h2>\n\n                          <p>18 de Janeiro</p>\n\n                      </ion-item>\n\n                      <ion-card-content>\n\n                        <ion-card-title>\n\n                          Palestra: Como aprender a ler\n\n                          </ion-card-title>\n\n                        <p>\n\n                          Biblioteca Central da UFBA\n\n                        </p>\n\n                      </ion-card-content>\n\n                    </ion-card>\n\n                  </ion-col>\n\n         </ion-list>\n\n        </div>\n\n        <ion-fab right bottom>\n\n          <button ion-fab color="educandu_clight" (click)="createPost()"><ion-icon name="create"></ion-icon></button>\n\n      </ion-fab>\n\n</ion-content>'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\group\group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["f" /* NavController */]])
    ], GroupPage);
    return GroupPage;
}());

//# sourceMappingURL=group.js.map

/***/ })

});
//# sourceMappingURL=0.js.map
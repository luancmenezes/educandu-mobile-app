webpackJsonp([9],{

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tb2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Tb2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tb2Page = (function () {
    function Tb2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Tb2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tb2Page');
    };
    Tb2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tb2',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tb2\tb2.html"*/'<!--\n\n  Generated template for the Tb2Page page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>tb2</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tb2\tb2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Tb2Page);
    return Tb2Page;
}());

//# sourceMappingURL=tb2.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterPage = (function () {
    // Initialise module classes
    function RegisterPage(http, fb, navCtrl, toastCtrl) {
        this.http = http;
        this.fb = fb;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        // Create form builder validation rules
        this.form = fb.group({
            name: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required],
            date_birth: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required],
            institution: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required],
            course: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required],
            email: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].pattern("[^ @]*@[^ @]*")]),
            password: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(10), __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].minLength(4)])
        });
    }
    RegisterPage.prototype.createEntry = function (payload) {
        var _this = this;
        var options = payload;
        var url = 'http://localhost:3000/usuarios';
        return this.http.post(url, options)
            .subscribe(function (data) {
            _this.sendNotification("Cadastrado com sucesso!");
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
        }, function (error) {
            _this.sendNotification('Falha ao cadastrar!');
            console.log(error);
        });
    };
    RegisterPage.prototype.saveEntry = function () {
        var payload = { name: '', date_birth: '', institution: '', course: '', email: '', password: '' };
        payload.name = this.form.controls["name"].value;
        payload.date_birth = this.form.controls["date_birth"].value;
        payload.institution = this.form.controls["institution"].value;
        payload.course = this.form.controls["course"].value;
        payload.email = this.form.controls["email"].value;
        payload.password = this.form.controls["password"].value;
        this.createEntry(payload);
    };
    RegisterPage.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\register\register.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>Registre-se</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n  \n\n<ion-content padding id="register">\n\n  <ion-grid>\n\n    <div *ngIf="!hideForm">\n\n      <form [formGroup]="form">\n\n\n\n        <ion-list>\n\n\n\n          <ion-item class="box-register">\n\n            <ion-input\n\n              class="register_input"\n\n              type="text"\n\n              [(ngModel)]="name"\n\n              formControlName="name"\n\n              placeholder="Digite seu nome">     \n\n            </ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item class="box-register">\n\n            <ion-input\n\n              class="register_input"\n\n               type="text"\n\n               placeholder="Data de nascimento"\n\n               [(ngModel)]="date_birth"\n\n               formControlName="date_birth">\n\n            </ion-input>\n\n         </ion-item>\n\n\n\n          <ion-item class="box-register">\n\n            <ion-input\n\n              class="register_input"\n\n              type="text"\n\n              [(ngModel)]="institution"\n\n              formControlName="institution"\n\n              placeholder="Instituição">     \n\n            </ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item class="box-register">\n\n            <ion-input\n\n              class="register_input"\n\n               type="text"\n\n               placeholder="Curso"\n\n               [(ngModel)]="course"\n\n               formControlName="course">\n\n            </ion-input>\n\n          </ion-item>\n\n        \n\n          <ion-item class="box-register">\n\n            <ion-input\n\n                class="register_input"\n\n                type="text"\n\n                [(ngModel)]="email"\n\n                formControlName="email"\n\n                placeholder="Email">                      \n\n            </ion-input>\n\n          </ion-item>\n\n\n\n          <ion-item class="box-register">\n\n            <ion-input\n\n                class="register_input"\n\n                type="password"\n\n                [(ngModel)]="password"\n\n                formControlName="password"\n\n                placeholder="Senha">                      \n\n            </ion-input>\n\n          </ion-item>\n\n\n\n        \n\n          \n\n          <button [color]="myColor" ion-button block (click)="saveEntry()" [disabled]="!form.valid">Registre-se</button>\n\n        </ion-list>\n\n      </form>\n\n    </div>\n\n  </ion-grid>\n\n</ion-content>\n\n  \n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\register\register.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ToastController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 117:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 117;

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/comment/comment.module": [
		288,
		3
	],
	"../pages/create-group/create-group.module": [
		289,
		8
	],
	"../pages/create-post/create-post.module": [
		290,
		1
	],
	"../pages/group/group.module": [
		291,
		0
	],
	"../pages/login/login.module": [
		292,
		7
	],
	"../pages/register/register.module": [
		293,
		6
	],
	"../pages/search/search.module": [
		294,
		2
	],
	"../pages/tb1/tb1.module": [
		295,
		5
	],
	"../pages/tb2/tb2.module": [
		296,
		4
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 158;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__profile_profile__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(161);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MenuPage = (function () {
    function MenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */];
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['person', 'construct', 'contacts', 'people', 'exit'];
        this.namePages = ['Meu Perfil', 'Configurações', 'Amigos', 'Grupos', 'Sair'];
        this.pages = ['ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage'];
        this.items = [];
        for (var i = 0; i < 5; i++) {
            this.items.push({
                page: this.pages[i],
                title: this.namePages[i],
                icon: this.icons[i],
            });
        }
    }
    MenuPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!  
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__profile_profile__["a" /* ProfilePage */]);
        //  this.navCtrl.push(item.page, {
        //    item: item
        //    });
    };
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\menu\menu.html"*/'<ion-menu [content]="content">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n \n\n  <ion-content>\n\n    <ion-list>\n\n      <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n\n        <ion-icon [name]="item.icon" item-start></ion-icon>\n\n        {{item.title}}\n\n        <div class="item-note" item-end>\n\n          {{item.note}}\n\n        </div>\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n</ion-menu>\n\n \n\n<!-- main navigation -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\menu\menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfilePage = (function () {
    function ProfilePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.selectedSegment = "second";
        this.slides = [];
    }
    ProfilePage.prototype.onSegmentChanged = function (segmentButton) {
        console.log("Segment changed to", segmentButton.value);
        var selectedIndex = this.slides.findIndex(function (slide) {
            return slide.id === segmentButton.value;
        });
    };
    ProfilePage.prototype.onSlideChanged = function (slider) {
        console.log('Slide changed');
        var currentSlide = this.slides[slider.activeIndex];
        this.selectedSegment = currentSlide.id;
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\profile\profile.html"*/'<ion-header>\n\n  <ion-navbar color="educandu_dark">\n\n      <ion-title>\n\n        Perfil\n\n      </ion-title>\n\n  </ion-navbar>\n\n  \n\n  <shrinking-segment-header scrollArea="myContent" headerHeight="150">\n\n    <ion-grid class="grid_content">\n\n      <ion-row>\n\n        <ion-col col-2></ion-col>\n\n        <ion-col col-8 > <img id="perfil_photo"src="assets/icon/doria.jpg" /></ion-col>\n\n        <ion-col col-2></ion-col>\n\n      </ion-row>\n\n      <ion-row>\n\n        <ion-col col-2></ion-col>\n\n        <ion-col col-8 id="perfil_text_col">  \n\n          <p class="perfil_text">Tiago Dória <br>Universidade Federal da Bahia</p>\n\n          \n\n        </ion-col>\n\n        <ion-col col-2></ion-col>\n\n      </ion-row>\n\n    \n\n    </ion-grid>\n\n  </shrinking-segment-header>\n\n\n\n  <ion-toolbar color="educandu_light" mode="md">\n\n    <ion-segment class="segment" color="dark" mode="md" [(ngModel)]="selectedSegment"  >\n\n      <ion-segment-button  value="first">\n\n        <ion-icon name="folder-open"></ion-icon>\n\n      </ion-segment-button>\n\n      <ion-segment-button value="second">\n\n        <ion-icon name="contacts"></ion-icon>\n\n      </ion-segment-button>\n\n      <ion-segment-button value="third">\n\n        <ion-icon name="albums"></ion-icon>\n\n      </ion-segment-button>\n\n    </ion-segment>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content >\n\n        <!-- <ion-slides #mySlider (ionDidChange)="onSlideChanged($event)">\n\n          <ion-slide *ngFor="let slide of slides">\n\n            {{ slide.title }}\n\n          </ion-slide>\n\n      </ion-slides> -->\n\n    \n\n      <div [ngSwitch]="selectedSegment">\n\n          <ion-list *ngSwitchCase="\'first\'">\n\n              <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Cothing</h2>\n\n               </ion-item>\n\n               <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Watches</h2>\n\n               </ion-item>\n\n               <ion-item>\n\n                 <ion-thumbnail item-left>\n\n                   <img src="/img/women-image1.png">\n\n                 </ion-thumbnail>\n\n                 <h2>Footwear</h2>\n\n               </ion-item>\n\n            </ion-list>\n\n          <ion-row *ngSwitchCase="\'second\'">\n\n              <ion-col col-12>\n\n                <ion-card>\n\n                  <ion-item>\n\n                    <ion-avatar item-start>\n\n                      <img src="assets/icon/perfil.jpg">\n\n                    </ion-avatar>\n\n                      <h2>Sahusa Carlos</h2>\n\n                      <p>18 de Janeiro</p>\n\n                  </ion-item>\n\n                  <ion-card-content>\n\n                    <ion-card-title>\n\n                      Recomendação de Livro\n\n                      </ion-card-title>\n\n                    <p>\n\n                      Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n\n                    </p>\n\n                  </ion-card-content>\n\n                </ion-card>\n\n              </ion-col>\n\n              <ion-col col-12>\n\n                  <ion-card>\n\n                    <ion-item>\n\n                      <ion-avatar item-start>\n\n                        <img src="assets/icon/perfil.jpg">\n\n                      </ion-avatar>\n\n                        <h2>Sahusa Carlos</h2>\n\n                        <p>18 de Janeiro</p>\n\n                    </ion-item>\n\n                    <ion-card-content>\n\n                      <ion-card-title>\n\n                        Recomendação de Livro\n\n                        </ion-card-title>\n\n                      <p>\n\n                        Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n\n                      </p>\n\n                    </ion-card-content>\n\n                  </ion-card>\n\n                </ion-col>\n\n            </ion-row>\n\n          <ion-list *ngSwitchCase="\'third\'">\n\n              <ion-col col-12>\n\n                  <ion-card>\n\n                    <ion-item>\n\n                      <ion-avatar item-start>\n\n                        <img src="assets/icon/doria.jpg">\n\n                      </ion-avatar>\n\n                        <h2>Sahusa Carlos</h2>\n\n                        <p>18 de Janeiro</p>\n\n                    </ion-item>\n\n                    <ion-card-content>\n\n                      <ion-card-title>\n\n                          Campus Mobile\n\n                        </ion-card-title>\n\n                      <p>\n\n                        Para quem gosta de empreender\n\n                      </p>\n\n                    </ion-card-content>\n\n                  </ion-card>\n\n                </ion-col>\n\n                <ion-col col-12>\n\n                    <ion-card>\n\n                      <ion-item>\n\n                        <ion-avatar item-start>\n\n                          <img src="assets/icon/doria.jpg">\n\n                        </ion-avatar>\n\n                          <h2>Sahusa Carlos</h2>\n\n                          <p>18 de Janeiro</p>\n\n                      </ion-item>\n\n                      <ion-card-content>\n\n                        <ion-card-title>\n\n                          Palestra: Como aprender a ler\n\n                          </ion-card-title>\n\n                        <p>\n\n                          Biblioteca Central da UFBA\n\n                        </p>\n\n                      </ion-card-content>\n\n                    </ion-card>\n\n                  </ion-col>\n\n         </ion-list>\n\n        </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_home__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__tb2_tb2__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__create_group_create_group__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = (function () {
    function TabsPage(modalCtrl) {
        this.modalCtrl = modalCtrl;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_0__home_home__["a" /* HomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__create_group_create_group__["a" /* CreateGroupPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__tb2_tb2__["a" /* Tb2Page */];
    }
    // openModal(){
    //   console.log("entrou");
    //   let modalPage = this.modalCtrl.create(CreateGroupPage); 
    //   modalPage.present();
    // }    
    TabsPage.prototype.ionViewDidLoad = function () {
        this.firstTabIcon = 'first'; // or 'first-changed'
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tabs\tabs.html"*/'\n\n<ion-tabs color="educandu" >\n\n  <ion-tab [root]="tab1Root" tabTitle="Inicio" tabIcon="home"></ion-tab>\n\n  <ion-tab [root]="tab1Root" tabTitle="Pesquisar" tabIcon="md-search"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabTitle="Grupo" tabIcon="add-circle"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="Mural" tabIcon="md-create"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabTitle="Mapa" tabIcon="md-pin"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["e" /* ModalController */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__create_group_create_group__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage.prototype.createGroup = function () {
        console.log("Teste");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__create_group_create_group__["a" /* CreateGroupPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      <ion-row>\n        <ion-col col-4><button ion-button menuToggle id="icon-lef"><ion-icon ios="ios-menu" md="md-menu" id="icon-lef"></ion-icon></button></ion-col>\n        <ion-col col-4  id="name">Educandu!</ion-col>\n        <ion-col col-4><ion-icon ios="ios-calendar" md="md-calendar" id="icon-right"></ion-icon></ion-col>\n      </ion-row>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n    <ion-row>\n        <ion-col col-12>\n          <ion-card>\n            <ion-item>\n              <ion-avatar item-start>\n                <img src="assets/icon/perfil.jpg">\n              </ion-avatar>\n                <h2>Luan Menezes</h2>\n                <p>18 de Janeiro</p>\n            </ion-item>\n            <ion-card-content>\n              <ion-card-title>\n                Recomendação de Livro\n                </ion-card-title>\n              <p>\n                Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n              </p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n        <ion-col col-12>\n            <ion-card>\n              <ion-item>\n                <ion-avatar item-start>\n                  <img src="assets/icon/perfil.jpg">\n                </ion-avatar>\n                  <h2>Luan Menezes</h2>\n                  <p>18 de Janeiro</p>\n              </ion-item>\n              <ion-card-content>\n                <ion-card-title>\n                  Nine Inch Nails Live\n                  </ion-card-title>\n                <p>\n                  The most popular industrial group ever, and largely\n                  responsible for bringing the music to a mass audience.\n                </p>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n          <ion-col col-12>\n              <ion-card>\n                <ion-item>\n                  <ion-avatar item-start>\n                    <img src="assets/icon/perfil.jpg">\n                  </ion-avatar>\n                    <h2>Luan Menezes</h2>\n                    <p>18 de Janeiro</p>\n                </ion-item>\n                <ion-card-content>\n                  <ion-card-title>\n                    Nine Inch Nails Live\n                    </ion-card-title>\n                  <p>\n                    The most popular industrial group ever, and largely\n                    responsible for bringing the music to a mass audience.\n                  </p>\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n            <ion-col col-12>\n                <ion-card>\n                  <ion-item>\n                    <ion-avatar item-start>\n                      <img src="assets/icon/perfil.jpg">\n                    </ion-avatar>\n                      <h2>Luan Menezes</h2>\n                      <p>18 de Janeiro</p>\n                  </ion-item>\n                  <ion-card-content>\n                    <ion-card-title>\n                      Nine Inch Nails Live\n                      </ion-card-title>\n                    <p>\n                      The most popular industrial group ever, and largely\n                      responsible for bringing the music to a mass audience.\n                    </p>\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n              <ion-col col-12>\n                  <ion-card>\n                    <ion-item>\n                      <ion-avatar item-start>\n                        <img src="assets/icon/perfil.jpg">\n                      </ion-avatar>\n                        <h2>Luan Menezes</h2>\n                        <p>18 de Janeiro</p>\n                    </ion-item>\n                    <ion-card-content>\n                      <ion-card-title>\n                        Nine Inch Nails Live\n                        </ion-card-title>\n                      <p>\n                        The most popular industrial group ever, and largely\n                        responsible for bringing the music to a mass audience.\n                      </p>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n      </ion-row>\n  <ion-fab right bottom>\n    <button ion-fab color="educandu_clight"><ion-icon name="add"></ion-icon></button>\n    <ion-fab-list side="top">\n      <button ion-fab (click)="createGroup()"><ion-icon name="pin"></ion-icon></button>\n      <button ion-fab (click)="createGroup()"><ion-icon name="people"></ion-icon></button>\n    </ion-fab-list>\n</ion-fab>\n</ion-content>\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tb1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Tb1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tb1Page = (function () {
    function Tb1Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Tb1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tb1Page');
    };
    Tb1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tb1',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tb1\tb1.html"*/'<!--\n\n  Generated template for the Tb1Page page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>tb1</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\tb1\tb1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], Tb1Page);
    return Tb1Page;
}());

//# sourceMappingURL=tb1.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(230);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_create_group_create_group__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_shrinking_segment_header_shrinking_segment_header__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tb1_tb1__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tb2_tb2__ = __webpack_require__(103);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_register_register__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__providers_authService__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_http__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_4__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tb2_tb2__["a" /* Tb2Page */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tb1_tb1__["a" /* Tb1Page */],
                __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_1__components_shrinking_segment_header_shrinking_segment_header__["a" /* ShrinkingSegmentHeader */],
                __WEBPACK_IMPORTED_MODULE_0__pages_create_group_create_group__["a" /* CreateGroupPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_18__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/comment/comment.module#CommentPageModule', name: 'CommentPage', segment: 'comment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-group/create-group.module#CreateGroupPageModule', name: 'CreateGroupPage', segment: 'create-group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/create-post/create-post.module#CreatePostPageModule', name: 'CreatePostPage', segment: 'create-post', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/group/group.module#GroupPageModule', name: 'GroupPage', segment: 'group', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tb1/tb1.module#Tb1PageModule', name: 'Tb1Page', segment: 'tb1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tb2/tb2.module#Tb2PageModule', name: 'Tb2Page', segment: 'tb2', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tb1_tb1__["a" /* Tb1Page */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tb2_tb2__["a" /* Tb2Page */],
                __WEBPACK_IMPORTED_MODULE_15__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_0__pages_create_group_create_group__["a" /* CreateGroupPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_17__providers_authService__["a" /* AuthService */],
                { provide: __WEBPACK_IMPORTED_MODULE_4__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 277:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShrinkingSegmentHeader; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShrinkingSegmentHeader = (function () {
    function ShrinkingSegmentHeader(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    ShrinkingSegmentHeader.prototype.ngAfterViewInit = function () {
        this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');
    };
    ShrinkingSegmentHeader.prototype.resizeHeader = function (ev) {
        var _this = this;
        ev.domWrite(function () {
            _this.newHeaderHeight = _this.headerHeight - ev.scrollTop;
            if (_this.newHeaderHeight < 0) {
                _this.newHeaderHeight = 0;
            }
            _this.renderer.setElementStyle(_this.element.nativeElement, 'height', _this.newHeaderHeight + 'px');
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('scrollArea'),
        __metadata("design:type", Object)
    ], ShrinkingSegmentHeader.prototype, "scrollArea", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('headerHeight'),
        __metadata("design:type", Number)
    ], ShrinkingSegmentHeader.prototype, "headerHeight", void 0);
    ShrinkingSegmentHeader = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'shrinking-segment-header',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\components\shrinking-segment-header\shrinking-segment-header.html"*/'<ng-content></ng-content>'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\components\shrinking-segment-header\shrinking-segment-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], ShrinkingSegmentHeader);
    return ShrinkingSegmentHeader;
}());

//# sourceMappingURL=shrinking-segment-header.js.map

/***/ }),

/***/ 285:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_login_login__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(202);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_0__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var apiUrl = 'http://localhost/ionic/api/';
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        console.log('Hello AuthService Provider');
    }
    AuthService.prototype.postData = function (credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
            _this.http.post(apiUrl + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], AuthService);
    return AuthService;
}());

//# sourceMappingURL=authService.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateGroupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(14);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CreateGroupPage = (function () {
    // Initialise module classes
    function CreateGroupPage(navCtrl, http, NP, fb, toastCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.NP = NP;
        this.fb = fb;
        this.toastCtrl = toastCtrl;
        this.recordID = null;
        this.baseURI = "http://localhost/ionic/grupos/";
        this.isEdited = false;
        this.hideForm = false;
        this.form = fb.group({
            "university": ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].requiredTrue],
            "materia": ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required],
            "semester": ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required],
            "teacher": ["", __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required]
        });
    }
    CreateGroupPage.prototype.ionViewWillEnter = function () {
        if (this.NP.get("record")) {
            this.isEdited = true;
            this.selectEntry(this.NP.get("record"));
        }
        else {
            this.isEdited = false;
            this.pageTitle = 'Create entry';
        }
    };
    CreateGroupPage.prototype.selectEntry = function (item) {
        this.groupUniversity = item.university;
        this.groupMaterial = item.materia;
        this.groupTeacher = item.teacher;
        this.groupSemester = item.semester;
        this.recordID = item.id;
    };
    CreateGroupPage.prototype.createEntry = function (university, materia, semester, teacher) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' }), options = { "key": "create", "university": university, "materia": materia, "semester": semester, "teacher": teacher }, url = this.baseURI + "manage-data.php";
        this.http.post(url, JSON.stringify(options), headers)
            .subscribe(function (data) {
            _this.hideForm = true;
            _this.sendNotification("Congratulations the group of: " + university + " was successfully added");
        }, function (error) {
            _this.sendNotification('Something went wrong!');
        });
    };
    CreateGroupPage.prototype.saveEntry = function () {
        var university = this.form.controls["university"].value, materia = this.form.controls["materia"].value, semester = this.form.controls["semester"].value, teacher = this.form.controls["teacher"].value;
        if (!this.isEdited) {
            this.createEntry(university, materia, semester, teacher);
        }
    };
    CreateGroupPage.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    CreateGroupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-create-group',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\create-group\create-group.html"*/'<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Crie um Grupo</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding id="register">\n\n   <ion-grid>\n\n    <div *ngIf="!hideForm">\n\n           <form [formGroup]="form" (ngSubmit)="saveEntry()">\n\n\n\n              <ion-list>\n\n                    <ion-item class="box-register">\n\n                       <ion-input\n\n                         class="register_input"\n\n                          type="text"\n\n                         \n\n                          placeholder="Universidade Federal da Bahia"\n\n                          formControlName="university"\n\n                          [(ngModel)]="groupUniversity"></ion-input>\n\n                    </ion-item>\n\n                    <ion-item class="box-register">\n\n                       <ion-input\n\n                         class="register_input"\n\n                          type="text"\n\n                          placeholder="Matéria"\n\n                          formControlName="materia"\n\n                          [(ngModel)]="groupMaterial"></ion-input>\n\n                    </ion-item>\n\n\n\n                    <ion-item class="box-register">\n\n                       <ion-input\n\n                         class="register_input"\n\n                          type="text"\n\n                          placeholder="Digite seu Professor"\n\n                          formControlName="teacher"\n\n                          [(ngModel)]="groupTeacher"></ion-input>\n\n                    </ion-item>\n\n\n\n                    <ion-item class="box-register">\n\n                       <ion-input\n\n                         class="register_input"\n\n                          type="text"\n\n                          placeholder="Semestre"\n\n                          formControlName="semester"\n\n                          [(ngModel)]="groupSemester"></ion-input>\n\n                    </ion-item>\n\n                    <button color="educandu_dark" ion-button block >Criar</button>\n\n\n\n              </ion-list>\n\n\n\n           </form>\n\n        </div>\n\n  </ion-grid>\n\n\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\create-group\create-group.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["f" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* ToastController */]])
    ], CreateGroupPage);
    return CreateGroupPage;
}());

//# sourceMappingURL=create-group.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_menu__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_register_register__ = __webpack_require__(104);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.myColor = 'educandu';
        this.user = {};
    }
    LoginPage.prototype.doLogin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */]);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.pushPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\login\login.html"*/'<ion-content padding id="login">\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col-3></ion-col>\n\n      <ion-col col-6>\n\n        <img src="assets/imgs/educando_logo.png" class="logo"/>\n\n      </ion-col>\n\n      <ion-col col-3></ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col  col-12>\n\n         <ion-list >\n\n           <ion-item class="box-login">\n\n             <ion-input class="login_input" [(ngModel)]="user.email" placeholder="Email" type="email"></ion-input>\n\n           </ion-item>\n\n           <ion-item class="box-login" >\n\n             <ion-input class="login_input" [(ngModel)]="user.pass" placeholder="Senha" type="password"></ion-input>\n\n           </ion-item>\n\n         </ion-list>\n\n       </ion-col>\n\n    </ion-row>\n\n    <button ion-button color="educandu_dark" (click)="doLogin()">Entrar</button>\n\n  </ion-grid>\n\n  <hr class="hr-text" data-content="ou">\n\n  <ion-grid>\n\n    <ion-row>\n\n      <button color="educandu_dark" ion-button block (click)="pushPage()">Registre-se</button>\n\n      <ion-col col-6>\n\n        <button\n\n               class="button"\n\n               ion-button\n\n               block\n\n               padding-vertical\n\n               color="primary"\n\n               (click)="LoginFacebook()">\n\n                <ion-icon ios="logo-facebook" md="logo-facebook"></ion-icon>\n\n                <p>acebook</p>\n\n            </button>\n\n      </ion-col>\n\n      <ion-col col-6>\n\n        <button\n\n               class="button"\n\n               ion-button\n\n\n\n               block\n\n               padding-vertical\n\n               block outline\n\n               color="google"\n\n               (click)="LoginGoogle()">\n\n\n\n                  <ion-icon ios="logo-google" md="logo-google"></ion-icon>\n\n                  oogle\n\n            </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\tiago\Music\educandu-mobile-app\mobile\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[208]);
//# sourceMappingURL=main.js.map
webpackJsonp([4],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tb2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Tb2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tb2Page = (function () {
    function Tb2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Tb2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tb2Page');
    };
    Tb2Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tb2',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tb2/tb2.html"*/'<!--\n  Generated template for the Tb2Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>tb2</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tb2/tb2.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], Tb2Page);
    return Tb2Page;
}());

//# sourceMappingURL=tb2.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RegisterPage = (function () {
    // Initialise module classes
    function RegisterPage(navCtrl, http, NP, fb, toastCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.NP = NP;
        this.fb = fb;
        this.toastCtrl = toastCtrl;
        this.recordID = null;
        this.baseURI = "http://localhost/ionic/";
        // Create form builder validation rules
        this.form = fb.group({
            "name": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            "date_birth": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            "institution": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            "course": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            "email": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            "password": ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    RegisterPage.prototype.ionViewWillEnter = function () {
        if (this.NP.get("record")) {
            //this.isEdited      = true;
            this.selectEntry(this.NP.get("record"));
            //this.pageTitle     = 'Amend entry';
        }
        else {
            //this.isEdited      = false;
            //this.pageTitle     = 'Create entry';
        }
    };
    RegisterPage.prototype.selectEntry = function (item) {
        this.userName = item.name;
        this.userDate = item.date_birth;
        this.userInstitution = item.institution;
        this.userCourse = item.course;
        this.userEmail = item.email;
        this.userPassword = item.password;
    };
    RegisterPage.prototype.createEntry = function (name, date_birth, institution, course, email, password) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/json' }), options = { "key": "create", "name": name, "date_birth": date_birth, "institution": institution, "course": course, "email": email, "password": password }, url = this.baseURI + "manage-data.php";
        this.http.post(url, JSON.stringify(options), headers)
            .subscribe(function (data) {
            // If the request was successful notify the user
            _this.sendNotification("Congratulations the user: " + name + " was successfully added");
        }, function (error) {
            _this.sendNotification('Something went wrong!');
        });
    };
    RegisterPage.prototype.saveEntry = function () {
        var name = this.form.controls["name"].value, date_birth = this.form.controls["date_birth"].value, institution = this.form.controls["institution"].value, course = this.form.controls["course"].value, email = this.form.controls["email"].value, password = this.form.controls["password"].value;
    };
    RegisterPage.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-register',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/register/register.html"*/'<ion-header>\n\n    <ion-navbar>\n      <ion-title>Registre-se</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content padding id="register">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-3></ion-col>\n        <ion-col col-6>\n          <img src="assets/imgs/educando_logo.png" class="logo"/>\n        </ion-col>\n        <ion-col col-3></ion-col>\n      </ion-row>\n    </ion-grid>\n  \n  \n    <ion-grid>\n      <div *ngIf="!hideForm">\n             <form [formGroup]="form" >\n  \n                <ion-list>\n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="text"\n                            placeholder="Digite seu nome"\n                            formControlName="name"\n                            [(ngModel)]="userName"></ion-input>\n                      </ion-item>\n  \n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="text"\n                            placeholder="Data de nascimento"\n                            formControlName="date_birth"\n                            [(ngModel)]="userDate"></ion-input>\n                      </ion-item>\n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="text"\n                            placeholder="Instituição"\n                            formControlName="institution"\n                            [(ngModel)]="userInstitution"></ion-input>\n                      </ion-item>\n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="text"\n                            placeholder="Curso"\n                            formControlName="course"\n                            [(ngModel)]="userCourse"></ion-input>\n                      </ion-item>\n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="text"\n                            placeholder="Email"\n                            formControlName="email"\n                            [(ngModel)]="userEmail"></ion-input>\n                      </ion-item>\n  \n                      <ion-item class="box-register">\n                         <ion-input\n                           class="register_input"\n                            type="password"\n                            placeholder="Password"\n                            formControlName="password"\n                            [(ngModel)]="userPassword"></ion-input>\n                      </ion-item>\n                      <button [color]="myColor" ion-button block (click)="saveEntry()" [disabled]="!form.valid">Registre-se</button>\n  \n                </ion-list>\n  \n             </form>\n          </div>\n    </ion-grid>\n  \n  \n  </ion-content>\n  '/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ToastController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 114;

/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/login/login.module": [
		285,
		3
	],
	"../pages/register/register.module": [
		286,
		2
	],
	"../pages/tb1/tb1.module": [
		287,
		1
	],
	"../pages/tb2/tb2.module": [
		288,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 157;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__profile_profile__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MenuPage = (function () {
    function MenuPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */];
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['person', 'construct', 'contacts', 'people', 'exit'];
        this.namePages = ['Meu Perfil', 'Configurações', 'Amigos', 'Grupos', 'Sair'];
        this.pages = ['ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage', 'ProfilePage'];
        this.items = [];
        for (var i = 0; i < 5; i++) {
            this.items.push({
                page: this.pages[i],
                title: this.namePages[i],
                icon: this.icons[i],
            });
        }
    }
    MenuPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!  
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__profile_profile__["a" /* ProfilePage */]);
        //  this.navCtrl.push(item.page, {
        //    item: item
        //    });
    };
    MenuPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MenuPage');
    };
    MenuPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-menu',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/menu/menu.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n \n  <ion-content>\n    <ion-list>\n      <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n        <ion-icon [name]="item.icon" item-start></ion-icon>\n        {{item.title}}\n        <div class="item-note" item-end>\n          {{item.note}}\n        </div>\n      </button>\n    </ion-list>\n  </ion-content>\n</ion-menu>\n \n<!-- main navigation -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/menu/menu.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* NavParams */]])
    ], MenuPage);
    return MenuPage;
}());

//# sourceMappingURL=menu.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfilePage = (function () {
    function ProfilePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.selectedSegment = "second";
        this.slides = [];
    }
    ProfilePage.prototype.onSegmentChanged = function (segmentButton) {
        console.log("Segment changed to", segmentButton.value);
        var selectedIndex = this.slides.findIndex(function (slide) {
            return slide.id === segmentButton.value;
        });
    };
    ProfilePage.prototype.onSlideChanged = function (slider) {
        console.log('Slide changed');
        var currentSlide = this.slides[slider.activeIndex];
        this.selectedSegment = currentSlide.id;
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar color="educandu_dark">\n      <ion-title>\n        Perfil\n      </ion-title>\n  </ion-navbar>\n  \n  <shrinking-segment-header scrollArea="myContent" headerHeight="150">\n    <ion-grid class="grid_content">\n      <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col col-8 > <img id="perfil_photo"src="assets/icon/doria.jpg" /></ion-col>\n        <ion-col col-2></ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-2></ion-col>\n        <ion-col col-8 id="perfil_text_col">  \n          <p class="perfil_text">Tiago Dória <br>Universidade Federal da Bahia</p>\n          \n        </ion-col>\n        <ion-col col-2></ion-col>\n      </ion-row>\n    \n    </ion-grid>\n  </shrinking-segment-header>\n\n  <ion-toolbar color="educandu_light" mode="md">\n    <ion-segment class="segment" color="dark" mode="md" [(ngModel)]="selectedSegment"  >\n      <ion-segment-button  value="first">\n        <ion-icon name="folder-open"></ion-icon>\n      </ion-segment-button>\n      <ion-segment-button value="second">\n        <ion-icon name="contacts"></ion-icon>\n      </ion-segment-button>\n      <ion-segment-button value="third">\n        <ion-icon name="albums"></ion-icon>\n      </ion-segment-button>\n    </ion-segment>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content >\n        <!-- <ion-slides #mySlider (ionDidChange)="onSlideChanged($event)">\n          <ion-slide *ngFor="let slide of slides">\n            {{ slide.title }}\n          </ion-slide>\n      </ion-slides> -->\n    \n      <div [ngSwitch]="selectedSegment">\n          <ion-list *ngSwitchCase="\'first\'">\n              <ion-item>\n                 <ion-thumbnail item-left>\n                   <img src="img/women-image1.png">\n                 </ion-thumbnail>\n                 <h2>Cothing</h2>\n               </ion-item>\n               <ion-item>\n                 <ion-thumbnail item-left>\n                   <img src="img/women-image1.png">\n                 </ion-thumbnail>\n                 <h2>Watches</h2>\n               </ion-item>\n               <ion-item>\n                 <ion-thumbnail item-left>\n                   <img src="/img/women-image1.png">\n                 </ion-thumbnail>\n                 <h2>Footwear</h2>\n               </ion-item>\n            </ion-list>\n          <ion-row *ngSwitchCase="\'second\'">\n              <ion-col col-12>\n                <ion-card>\n                  <ion-item>\n                    <ion-avatar item-start>\n                      <img src="assets/icon/perfil.jpg">\n                    </ion-avatar>\n                      <h2>Sahusa Carlos</h2>\n                      <p>18 de Janeiro</p>\n                  </ion-item>\n                  <ion-card-content>\n                    <ion-card-title>\n                      Recomendação de Livro\n                      </ion-card-title>\n                    <p>\n                      Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n                    </p>\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n              <ion-col col-12>\n                  <ion-card>\n                    <ion-item>\n                      <ion-avatar item-start>\n                        <img src="assets/icon/perfil.jpg">\n                      </ion-avatar>\n                        <h2>Sahusa Carlos</h2>\n                        <p>18 de Janeiro</p>\n                    </ion-item>\n                    <ion-card-content>\n                      <ion-card-title>\n                        Recomendação de Livro\n                        </ion-card-title>\n                      <p>\n                        Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n                      </p>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n            </ion-row>\n          <ion-list *ngSwitchCase="\'third\'">\n              <ion-col col-12>\n                  <ion-card>\n                    <ion-item>\n                      <ion-avatar item-start>\n                        <img src="assets/icon/doria.jpg">\n                      </ion-avatar>\n                        <h2>Sahusa Carlos</h2>\n                        <p>18 de Janeiro</p>\n                    </ion-item>\n                    <ion-card-content>\n                      <ion-card-title>\n                          Campus Mobile\n                        </ion-card-title>\n                      <p>\n                        Para quem gosta de empreender\n                      </p>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-col>\n                <ion-col col-12>\n                    <ion-card>\n                      <ion-item>\n                        <ion-avatar item-start>\n                          <img src="assets/icon/doria.jpg">\n                        </ion-avatar>\n                          <h2>Sahusa Carlos</h2>\n                          <p>18 de Janeiro</p>\n                      </ion-item>\n                      <ion-card-content>\n                        <ion-card-title>\n                          Palestra: Como aprender a ler\n                          </ion-card-title>\n                        <p>\n                          Biblioteca Central da UFBA\n                        </p>\n                      </ion-card-content>\n                    </ion-card>\n                  </ion-col>\n         </ion-list>\n        </div>\n</ion-content>'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/profile/profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tb2_tb2__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabsPage = (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__tb2_tb2__["a" /* Tb2Page */];
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        this.firstTabIcon = 'first'; // or 'first-changed'
    };
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tabs/tabs.html"*/'\n<ion-tabs color="educandu" >\n  <ion-tab [root]="tab1Root" tabTitle="Inicio" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab1Root" tabTitle="Pesquisar" tabIcon="md-search"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Mural" tabIcon="md-create"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Mapa" tabIcon="md-pin"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title [color]="educandu">\n      <ion-row>\n        <ion-col col-4><button ion-button menuToggle id="icon-lef"><ion-icon ios="ios-menu" md="md-menu" id="icon-lef"></ion-icon></button></ion-col>\n        <ion-col col-4  id="name">Educandu!</ion-col>\n        <ion-col col-4><ion-icon ios="ios-calendar" md="md-calendar" id="icon-right"></ion-icon></ion-col>\n      </ion-row>\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <ion-row>\n    <ion-col col-12>\n      <ion-card>\n        <ion-item>\n          <ion-avatar item-start>\n            <img src="assets/icon/perfil.jpg">\n          </ion-avatar>\n            <h2>Luan Menezes</h2>\n            <p>18 de Janeiro</p>\n        </ion-item>\n        <ion-card-content>\n          <ion-card-title>\n            Recomendação de Livro\n            </ion-card-title>\n          <p>\n            Alguém conhece algum livro interessante de aprendizagem de máquina? Socorro!\n          </p>\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n    <ion-col col-12>\n        <ion-card>\n          <ion-item>\n            <ion-avatar item-start>\n              <img src="assets/icon/perfil.jpg">\n            </ion-avatar>\n              <h2>Luan Menezes</h2>\n              <p>18 de Janeiro</p>\n          </ion-item>\n          <ion-card-content>\n            <ion-card-title>\n              Nine Inch Nails Live\n              </ion-card-title>\n            <p>\n              The most popular industrial group ever, and largely\n              responsible for bringing the music to a mass audience.\n            </p>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n      <ion-col col-12>\n          <ion-card>\n            <ion-item>\n              <ion-avatar item-start>\n                <img src="assets/icon/perfil.jpg">\n              </ion-avatar>\n                <h2>Luan Menezes</h2>\n                <p>18 de Janeiro</p>\n            </ion-item>\n            <ion-card-content>\n              <ion-card-title>\n                Nine Inch Nails Live\n                </ion-card-title>\n              <p>\n                The most popular industrial group ever, and largely\n                responsible for bringing the music to a mass audience.\n              </p>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n        <ion-col col-12>\n            <ion-card>\n              <ion-item>\n                <ion-avatar item-start>\n                  <img src="assets/icon/perfil.jpg">\n                </ion-avatar>\n                  <h2>Luan Menezes</h2>\n                  <p>18 de Janeiro</p>\n              </ion-item>\n              <ion-card-content>\n                <ion-card-title>\n                  Nine Inch Nails Live\n                  </ion-card-title>\n                <p>\n                  The most popular industrial group ever, and largely\n                  responsible for bringing the music to a mass audience.\n                </p>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n          <ion-col col-12>\n              <ion-card>\n                <ion-item>\n                  <ion-avatar item-start>\n                    <img src="assets/icon/perfil.jpg">\n                  </ion-avatar>\n                    <h2>Luan Menezes</h2>\n                    <p>18 de Janeiro</p>\n                </ion-item>\n                <ion-card-content>\n                  <ion-card-title>\n                    Nine Inch Nails Live\n                    </ion-card-title>\n                  <p>\n                    The most popular industrial group ever, and largely\n                    responsible for bringing the music to a mass audience.\n                  </p>\n                </ion-card-content>\n              </ion-card>\n            </ion-col>\n  </ion-row>\n</ion-content>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Tb1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Tb1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var Tb1Page = (function () {
    function Tb1Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Tb1Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Tb1Page');
    };
    Tb1Page = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tb1',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tb1/tb1.html"*/'<!--\n  Generated template for the Tb1Page page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>tb1</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/tb1/tb1.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], Tb1Page);
    return Tb1Page;
}());

//# sourceMappingURL=tb1.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(228);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_shrinking_segment_header_shrinking_segment_header__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_home_home__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tb1_tb1__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tb2_tb2__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_register_register__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__ = __webpack_require__(159);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tb2_tb2__["a" /* Tb2Page */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tb1_tb1__["a" /* Tb1Page */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_0__components_shrinking_segment_header_shrinking_segment_header__["a" /* ShrinkingSegmentHeader */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tb1/tb1.module#Tb1PageModule', name: 'Tb1Page', segment: 'tb1', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tb2/tb2.module#Tb2PageModule', name: 'Tb2Page', segment: 'tb2', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_menu_menu__["a" /* MenuPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tb1_tb1__["a" /* Tb1Page */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tb2_tb2__["a" /* Tb2Page */],
                __WEBPACK_IMPORTED_MODULE_14__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_profile_profile__["a" /* ProfilePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_3__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ShrinkingSegmentHeader; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ShrinkingSegmentHeader = (function () {
    function ShrinkingSegmentHeader(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    ShrinkingSegmentHeader.prototype.ngAfterViewInit = function () {
        this.renderer.setElementStyle(this.element.nativeElement, 'height', this.headerHeight + 'px');
    };
    ShrinkingSegmentHeader.prototype.resizeHeader = function (ev) {
        var _this = this;
        ev.domWrite(function () {
            _this.newHeaderHeight = _this.headerHeight - ev.scrollTop;
            if (_this.newHeaderHeight < 0) {
                _this.newHeaderHeight = 0;
            }
            _this.renderer.setElementStyle(_this.element.nativeElement, 'height', _this.newHeaderHeight + 'px');
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('scrollArea'),
        __metadata("design:type", Object)
    ], ShrinkingSegmentHeader.prototype, "scrollArea", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('headerHeight'),
        __metadata("design:type", Number)
    ], ShrinkingSegmentHeader.prototype, "headerHeight", void 0);
    ShrinkingSegmentHeader = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'shrinking-segment-header',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/components/shrinking-segment-header/shrinking-segment-header.html"*/'<ng-content></ng-content>'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/components/shrinking-segment-header/shrinking-segment-header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_0__angular_core__["V" /* Renderer */]])
    ], ShrinkingSegmentHeader);
    return ShrinkingSegmentHeader;
}());

//# sourceMappingURL=shrinking-segment-header.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_login_login__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_0__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_menu__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_register_register__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.myColor = 'educandu';
        this.user = {};
    }
    LoginPage.prototype.doLogin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__menu_menu__["a" /* MenuPage */]);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.pushPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/login/login.html"*/'<ion-content padding id="login">\n  <ion-grid>\n    <ion-row>\n      <ion-col col-3></ion-col>\n      <ion-col col-6>\n        <img src="assets/imgs/educando_logo.png" class="logo"/>\n      </ion-col>\n      <ion-col col-3></ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col  col-12>\n         <ion-list >\n           <ion-item class="box-login">\n             <ion-input class="login_input" [(ngModel)]="user.email" placeholder="Email" type="email"></ion-input>\n           </ion-item>\n           <ion-item class="box-login" >\n             <ion-input class="login_input" [(ngModel)]="user.pass" placeholder="Senha" type="password"></ion-input>\n           </ion-item>\n         </ion-list>\n       </ion-col>\n    </ion-row>\n    <button ion-button [color]="myColor" (click)="doLogin()">Entrar</button>\n  </ion-grid>\n  <hr class="hr-text" data-content="ou">\n  <ion-grid>\n    <ion-row>\n      <button [color]="myColor" ion-button block (click)="pushPage()">Registre-se</button>\n      <ion-col col-6>\n        <button\n               class="button"\n               ion-button\n               block\n               padding-vertical\n               color="primary"\n               (click)="LoginFacebook()">\n                <ion-icon ios="logo-facebook" md="logo-facebook"></ion-icon>\n                <p>acebook</p>\n            </button>\n      </ion-col>\n      <ion-col col-6>\n        <button\n               class="button"\n               ion-button\n\n               block\n               padding-vertical\n               block outline\n               color="google"\n               (click)="LoginGoogle()">\n\n                  <ion-icon ios="logo-google" md="logo-google"></ion-icon>\n                  oogle\n            </button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/luancmenezes/Documents/Educandu/educandu-mobile-app/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map